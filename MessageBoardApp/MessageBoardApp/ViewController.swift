//
//  ViewController.swift
//  MessageBoardApp
//
//  Created by Rastislav Smolen on 03/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import UIKit

class UserCellController: UITableViewCell
{
    
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var userNameLabel: UILabel!
   
}
class ViewController: UIViewController {
    var cellControll :UserCellController!
    var model : ViewModel!
    var users : UserElement!
    

    @IBOutlet var tableView: UITableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        model = ViewModel()
        cellControll = UserCellController()
        
      
        model.IntializeUserData {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
        
    }

}
    
    extension ViewController : UITableViewDataSource, UITableViewDelegate
    {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return model.userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell",for : indexPath) as? UserCellController
        
        cell?.userNameLabel.text = "\(model.userList[indexPath.row].userName)"
       // cell.detailTextLabel?.text = " \(model.albums[indexPath.row].artist)"
     
        return cell!
    }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            
              let vc = storyboard?.instantiateViewController(identifier: "UserBoardViewController") as? UserBoardViewController
            
            vc?.userName = "\(model.userList[indexPath.row].nameIRL)"
            vc?.emailAdress = "\(model.userList[indexPath.row].emailAdress)"
            vc?.zipcode = "\(model.userList[indexPath.row].zipcodeAdress)"
            vc?.city = "\(model.userList[indexPath.row].cityAdress)"
            vc?.street = "\(model.userList[indexPath.row].streetAdress)"
             // performSegue(withIdentifier: "ShowPost", sender: prepare)
            self.navigationController?.pushViewController(vc!, animated: true)

         //   print(cell.userNameLabel.text)
          //    print(indexPath)
          }
    
        
    }
