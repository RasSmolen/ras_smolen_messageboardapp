//
//  ViewModel.swift
//  MessageBoardApp
//
//  Created by Rastislav Smolen on 03/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import Foundation

struct UserElement: Codable {
    let id: Int
    let name :String
    let username :String
    let email: String
    let address: Address
    let phone, website: String
    let company: Company
}

struct Address: Codable {
    let street, suite, city, zipcode: String
    let geo: Geo
}

struct Geo: Codable {
    let lat, lng: String
}


struct Company: Codable {
    let name, catchPhrase, bs: String
}

typealias User = [UserElement]




struct Post: Codable {
    let userID, id: Int
    let title, body: String

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}
struct Comment: Codable {
    let postID, id: Int
    let name, email, body: String

    enum CodingKeys: String, CodingKey {
        case postID = "postId"
        case id, name, email, body
    }
}

typealias Comments = [Comment]


typealias Posts = [Post]

struct UserList
{
    var userName : String
    var nameIRL : String
    var emailAdress : String
    var streetAdress : String
    var cityAdress : String
    var zipcodeAdress : String
}
struct UserPost
{
    var postTitle : String
}
struct CommentNumber
{
    var comments: String
}
class ViewModel
{
    var userList = [UserList] ()
    var userPost = [UserPost]()
    var numberOfComments = [CommentNumber]()
    init()
    {
        
        numberOfComments = []
        userList = []
        userPost = []
    }

     func IntializeUserData(completion: @escaping () -> Void)
     {
        
     let jsonUrlString = "http://jsonplaceholder.typicode.com/users"
         guard let url = URL(string: jsonUrlString) else {return}
     
         URLSession.shared.dataTask(with: url)
         {(data,respondse,err) in
             
             guard let data = data else {return}
             
             do
             {
                 let  user = try JSONDecoder().decode(User.self, from: data)
                
                for element in user.self
                {
                    self.userList.append(UserList(userName: element.username, nameIRL: element.name, emailAdress: element.email, streetAdress: element.address.street, cityAdress: element.address.city, zipcodeAdress: element.address.zipcode))
                    
                }
                print(self.userList)
                 completion()
                 
             }catch
             {
                
                     print(error)
             }
         }.resume()
    
     }
    func IntializeUserPost(completion: @escaping () -> Void)
        {
           
        let jsonUrlString = "http://jsonplaceholder.typicode.com/posts"
            guard let url = URL(string: jsonUrlString) else {return}
        
            URLSession.shared.dataTask(with: url)
            {(data,respondse,err) in
                
                guard let data = data else {return}
                
                do
                {
                    let  user = try JSONDecoder().decode(Posts.self, from: data)
                   
                    for element in user.self
                   {
                    self.userPost.append(UserPost(postTitle: element.title))
                 //   let mappedValues =
// map the valuse of id and user id
                    
                   }
                   print(self.userPost)
                    completion()
                    
                }catch
                {
                   
                        print(error)
                }
            }.resume()
       
        }
    
    func IntializeComment(completion: @escaping () -> Void)
            {
               
            let jsonUrlString = "http://jsonplaceholder.typicode.com/posts"
                guard let url = URL(string: jsonUrlString) else {return}
            
                URLSession.shared.dataTask(with: url)
                {(data,respondse,err) in
                    
                    guard let data = data else {return}
                    
                    do
                    {
                        let  user = try JSONDecoder().decode(Comments.self, from: data)
                       
                        for element in user.self
                       {
                        self.numberOfComments.append(CommentNumber(comments: element.body))
                     //   let mappedValues =
    // map the valuse of id and user id
                        
                       }
                       print(self.userPost)
                        completion()
                        
                    }catch
                    {
                       
                            print(error)
                    }
                }.resume()
           
            }
         
      
     
  
}
