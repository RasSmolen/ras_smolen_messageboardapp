//
//  UserBoardViewController.swift
//  MessageBoardApp
//
//  Created by Rastislav Smolen on 03/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import UIKit

class UserBoardTableViewCellController :UITableViewCell
{
    @IBOutlet var postTitleLable: UILabel!
    
    @IBOutlet var commetLabel: UILabel!
    
    
}

class UserBoardViewController: UIViewController
    
    

{
    var model : ViewModel!
   var tableViewContent : ViewController!
    var cellController : UserCellController!
    
    var userName = ""
    var emailAdress = ""
//    struct AdressStruct
//    {
     
        var zipcode = ""
        var street = ""
        var city = ""
//    }
    
 //   var adress : [String]!
  //  var adressStruct = [AdressStruct] ()
    @IBOutlet var userNameLabel: UILabel!
    
    @IBOutlet var tableViewTwo: UITableView!
    
    @IBOutlet var adressLabel: UILabel!
    
    @IBOutlet var emailLabel: UILabel!
    
    override func viewDidLoad()
    {
        
      
        
        
        title = userName
        model = ViewModel()
        tableViewContent = ViewController()
        cellController  = UserCellController()
        super.viewDidLoad()
        userNameLabel.text = userName
        emailLabel.text = emailAdress
        adressLabel.text = ("\(street) - \(city) - \(zipcode)")
        
        model.IntializeComment{
                           DispatchQueue.main.async {
                       self.tableViewTwo.reloadData()
                   }
        }
        model.IntializeUserPost
            {
                   DispatchQueue.main.async {
                       self.tableViewTwo.reloadData()
                   }
        }
        
    }


}
extension UserBoardViewController : UITableViewDataSource, UITableViewDelegate
{
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
{
    return model.userPost.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
{
    let cell = tableView.dequeueReusableCell(withIdentifier: "ContentCell",for : indexPath) as? UserBoardTableViewCellController
   
    cell?.postTitleLable.text = "\(model.userPost[indexPath.row].postTitle)"
  //  cell?.commetLabel?.text = " \(model.numberOfComments[indexPath.row].comments)"
    //print(numberOfCommets)
 
    return cell!
}

}

